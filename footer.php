<footer>
  <div class="sub-foot">
    <div class="sub-holder">
      <div class="foot_area1">
        <?php dynamic_sidebar('f-area1'); ?>
      </div>
      <div class="foot_area2">
        <?php dynamic_sidebar('f-area2'); ?>
      </div>
      <div class="info">
        <?php dynamic_sidebar('f-area3') ?>
      </div>
      <div class="twitter">
        <?php dynamic_sidebar('f-area4') ?>
      </div>
    </div>
  </div>
  <div class="low-foot">
    <?php dynamic_sidebar('low-menu'); ?>
    <div class="low">
      <div class="socialhub">
        <a href=""><i class="fa fa-facebook"></i></a>
        <a href=""><i class="fa fa-twitter"></i></a>
        <a href=""><i class="fa fa-instagram"></i></a>
      </div>
      <p class="legal"> | &reg; Ecomarket Panama todos los derechos reservados</p>
    </div>
  </div>
  <?php wp_footer(); ?>
</footer>
</body>
</html>
