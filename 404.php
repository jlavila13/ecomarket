
<?php

    $post = get_post($_POST['id']);

?>
<div id="single-post post-<?php the_ID(); ?>">
  <?php while (have_posts()) : the_post(); ?>
  <i class="material-icons close">close</i>
  <figure>
    <span class="p-name">
      <h1><?php the_title(); ?></h1>
    </span>
    <?php the_post_thumbnail(); ?>
  </figure>
  <article>
    <p class="cat-tag">echo get_category();</p>
    <section>
        <p>
					<h1 class="page-title"><?php _e( '¡Aqui  no hay  nada!', 'twentythirteen' ); ?></h1>
					</header>
					<h2><?php _e( 'Es algo  embarazoso, no crees?', 'twentythirteen' ); ?></h2>
					<p><?php _e( 'Aquí al parecer no se encuentra lo que estas buscando. Que tal si buscas de nuevo?', 'twentythirteen' ); ?></p>

      </p>
    </section>
  </article>
  <?php endwhile; ?>
</div>
