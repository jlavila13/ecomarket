<aside class="sidecart">
  <button class="cart-button"><i class="flaticon-basket37"></i></button>
	<section>
		<?php echo do_shortcode('[woocommerce_cart]'); ?>
	</section>
</aside>
