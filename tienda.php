<?php /* Template name: tienda buu */ ?>
<?php get_header(); ?>
    <main class="tienda">
      <div class="cover">
        <?php
            $args = array(
              'post_type' => 'product',
              'posts_per_page' => 1
            );
            $loop = new WP_Query($args);
            while( $loop->have_posts() ) : $loop->the_post();
         ?>
        <div class="cover-holder">
          <figure><?php the_post_thumbnail(); ?></figure>
          <div class="txt">
            <p>
            	<h2><?php the_title(); ?></h2>
            	<p>
            	    <?php
                        global $product;
                        $attributes = $product->get_attributes();
                        if ( $product->has_weight() ) {
                            echo $product->get_weight();
                        }
                    ?>
                    Kg.
            	</p>
				<h1 class="precio">999,99 $</h1>
            </p>
            <div class="button-holder">
              <?php
                $id = get_the_ID();
                $add = sprintf(
                  '[add_to_cart id="%1$s"]',
                  $id
                );
              ?>
              <?php echo do_shortcode($add); ?>
            </div>
          </div>
        </div>
        <?php endwhile; wp_reset_query(); ?>
      </div>
      <section>
				<div class="price-range">
					<h3>precio</h3>
					<input type="range" name="" min="1" max="99" value="">
					<select class="" name="">
						<option value="">frutas</option>
						<option value="">frutas</option>
						<option value="">frutas</option>
						<option value="">frutas</option>
					</select>
					<button><i class="fa fa-sliders"></i>Filtrar</button>
				</div>
        <div class="top-nav">
          <?php echo do_shortcode('[top-nav]'); ?>
        </div>
        <div class="shoproll">
          <?php
              $args = array(
                'post_type' => 'product',
                'post_per_page' => 10
              );
              $loop = new WP_Query($args);
              while( $loop->have_posts() ) : $loop->the_post();
           ?>
            <article>
              <figure>
              <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
              </figure>
              <div class="desc">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p>
            	    <?php
                        global $product;
                        $attributes = $product->get_attributes();
                        if ( $product->has_weight() ) {
                            echo $product->get_weight();
                        }
                    ?>
                    Kg.
            	</p>
              </div>
              <div class="price-holder">
                <div class="button-holder">
                  <?php
                    $id = get_the_ID();
                    $add = sprintf(
                      '[add_to_cart id="%1$s"]',
                      $id
                    );
                  ?>
                  <?php echo do_shortcode($add); ?>
                </div>
              </div>
            </article>
        <?php endwhile; wp_reset_query(); ?>
        </div>
      </section>
    </main>
  <?php get_footer(); ?>
