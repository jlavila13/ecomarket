$(document).ready(function(){
  $('.feat-slide').flexslider({
    directionalNav: "true",
    animation: "slide",
    itemWidth: 250,
    minItems: 4
  });
  $('.related').flexslider({
    directionalNav: "true",
    animation: "slide",
    itemWidth: 250,
    minItems: 4
  });
  $('.cart-button').click(function(){
    $('.sidecart').toggleClass('swipe');
  });
  $('.p-link, .woocommerce-LoopProduct-link').click(function(){
       $('body').addClass('lock-body');
       $('.modal').addClass('show-modal');
       $('.modal').removeClass('hide-modal');
  });
  $('.fa-close').click(function(){
      $(this).parent().addClass('hide-modal');
      $('body').removeClass('lock-body');
      $(this).parent().removeClass('show-modal');
  });
  //funccion cantidad
  $('.cart').on('click', '.plus, .minus', function(e){
    var actual = $(this).parents('.cart').find('.qty').val();
    var boton = $(this).parents('.cart').find('.button');
    var cant = $(this).parents('article').find('.cant');
    var e = 1;
    var actual = parseInt(actual);
    var actual = actual + e;
    boton.attr('data-quantity', actual);
    cant.text(actual);
  });
});

//articulos - modal
$(document).on('click', '.p-link', function(){
    var post_link = $(this).attr("data-link");
    var link = post_link;
    $(this).find('article').bind(productModal(link));
    return false;
});

//infinite loop
  var count = 2;
  $(window).scroll(function(){
    var item = $('.loop' + count);
    if($(window).scrollTop() == $(document).height() - $(window).height()){
      loadArticle(count);
      count++;
    }
  });

  var pageNumber = count;
  var loopa = $('.loop' + pageNumber);
//realtime cart
;( function ( $ ) {
   "use strict";
  // Define the PHP function to call from here
   var data = {
     'action': 'mode_theme_update_mini_cart'
   };
   $.post(
     woocommerce_params.ajax_url, // The AJAX URL
     data, // Send our PHP function
     function(response){
       $('#mode-mini-cart').html(response); // Repopulate the specific element with the new content
     }
   );
  // Close anon function.
}( jQuery ) );

//funciones
function increment(){
  if ( ! String.prototype.getDecimals ) {
		String.prototype.getDecimals = function() {
			var num = this,
				match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
			if ( ! match ) {
				return 0;
			}
			return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
		}
	}

	function wcqi_refresh_quantity_increments(){
		$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" class="plus" />' ).prepend( '<input type="button" value="-" class="minus" />' );
	}

	$( '.loop'+count ).on( 'updated_wc_div', function() {
		wcqi_refresh_quantity_increments();
	} );

	$( '.loop'+count ).on( 'click', '.plus, .minus', function() {
		// Get values
		var $qty		= $( this ).closest( '.quantity' ).find( '.qty'),
			currentVal	= parseFloat( $qty.val() ),
			max			= parseFloat( $qty.attr( 'max' ) ),
			min			= parseFloat( $qty.attr( 'min' ) ),
			step		= $qty.attr( 'step' );

		// Format values
		if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
		if ( max === '' || max === 'NaN' ) max = '';
		if ( min === '' || min === 'NaN' ) min = 0;
		if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

		// Change the value
		if ( $( this ).is( '.plus' ) ) {
			if ( max && ( currentVal >= max ) ) {
				$qty.val( max );
			} else {
				$qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		} else {
			if ( min && ( currentVal <= min ) ) {
				$qty.val( min );
			} else if ( currentVal > 0 ) {
				$qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		}

		// Trigger change event
		$qty.trigger( 'change' );
	});

	wcqi_refresh_quantity_increments();
}

function cartUpdate(){
  $('.cart').on('click', '.plus, .minus', function(e){
    var actual = $(this).parents('.cart').find('.qty').val();
    var boton = $(this).parents('.cart').find('.button');
    var e = 1;
    var actual = parseInt(actual);
    var actual = actual + e;
    boton.attr('data-quantity', actual);
  });
}

function addInc(){
var loopa = $('.loop' + count);
    loopa.bind(increment());
    loopa.bind(cartUpdate());
    //loopa.bind(productModal());
};

function productModal(link){
    $.ajaxSetup({cache:false});
    $('body').addClass('lock-body');
    $('.modal').addClass('show-modal');
    $('.modal').removeClass('hide-modal');
    $(".p-container").html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>');
    $(".p-container").load(link);
    console.log(link);
}

function addItem(){
  var monto = $(this).parents('article').find('.qty').val();
  $(this).on('click', '.ajax_add_to_cart', function(){
    $(this).parents(article).find('.cant').text(monto);
  });
}

function loadArticle(pageNumber){
      $(".loader").fadeIn().append('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
      var str = '&pageNumber=' + pageNumber  + '&action=infinite_scroll';
      $.ajax({
          url: ajaxurl,
          type:'POST',
          data: str,
          success: function(html){
              $(".shoproll").append(html);   // This will be the div where our content will be loaded
              addInc();
          }, error: function() {
              console.log('nope theres nothing');

          }, complete: function(){
              $('.loader').fadeOut().find('i').remove();
          }
      });
      return false;
  }
