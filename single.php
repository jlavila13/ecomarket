<?php get_header(); ?>
<main class="single">
  <div class="top-nav">
    <?php echo do_shortcode('[top-nav]'); ?>
  </div>
  <?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <section>
    <div class="art-head">
      <h1><?php the_title(); ?></h1>
      <div class="meta">
        <p>publicado el: <?php the_time() ?> | en: <?php the_category(', '); ?></p>
      </div>
    </div>
    <article>
      <figure>
        <?php the_post_thumbnail(); ?>
      </figure>
      <div class="content">
        <?php the_content(); ?>
      </div>
    </article>
    <aside>
      <?php get_sidebar(); ?>
    </aside>
  </section>
  <?php endwhile; else : ?>
  <p><?php _e( 'Disculpa, no encontramos lo que buscas' ); ?></p>
<?php endif; wp_reset_query();?>
  <div class="feat-slide">
    <h1 class="title">Productos destacados</h1>
    <ul class="slides">
      <?php
          $args = array(
            'post_type' => 'product',
            'post_per_page' => 10
          );
          $loop = new WP_Query($args);
          while( $loop->have_posts() ) : $loop->the_post();
       ?>
      <li>
        <article>
          <figure>
          <a class="p-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
          </figure>
          <div class="desc">
            <h2><a class="p-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          </div>
          <div class="price-holder">
            <div class="button-holder">
              <?php
                $id = get_the_ID();
                $add = sprintf(
                  '[add_to_cart id="%1$s"]',
                  $id
                );
              ?>
              <?php echo do_shortcode($add); ?>
            </div>
          </div>
        </article>
      </li>
    <?php endwhile; wp_reset_query(); ?>
    </ul>
  </div>
  <div class="modal">
     <i class="fa fa-close"></i>
     <article class="p-container">

    </article>
  </div>
  </main>
<?php get_footer(); ?>
