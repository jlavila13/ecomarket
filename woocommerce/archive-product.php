<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div class="tienda">

	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

		<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

	<?php endif; ?>
	<?php
	/**
	* woocommerce_before_main_content hook.
	*
	* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
	* @hooked woocommerce_breadcrumb - 20
	*/
	do_action( 'woocommerce_before_main_content' );
	?>
	<?php
	/**
	* woocommerce_archive_description hook.
	*
	* @hooked woocommerce_taxonomy_archive_description - 10
	* @hooked woocommerce_product_archive_description - 10
	*/
	do_action( 'woocommerce_archive_description' );
	?>
	<div class="cover">
		<?php
				$args = array(
					'post_type' => 'product',
					'orderby' => 'rand',
					'posts_per_page' => 1
				);
				$loop = new WP_Query($args);
				while( $loop->have_posts() ) : $loop->the_post();
		 ?>
		<div class="cover-holder">
			<figure><?php the_post_thumbnail(); ?></figure>
			<div class="txt">
				<p>
					<h2><?php the_title(); ?></h2>
					<h1 class="precio">999,99 $</h1>
				</p>
				<div class="button-holder">
					<?php
						$id = get_the_ID();
						$add = sprintf(
							'[add_to_cart id="%1$s"]',
							$id
						);
					?>
					<?php echo do_shortcode($add); ?>
				</div>
			</div>
		</div>
		<?php endwhile; wp_reset_query(); ?>
	</div>

  <section>
    <div class="price-range">
      <div class="top-nav">
          <?php echo do_shortcode('[top-nav]'); ?>
      </div>
      <?php
        /**
         * woocommerce_before_shop_loop hook.
         *
         * @hooked woocommerce_result_count - 20
         * @hooked woocommerce_catalog_ordering - 30
         */
        echo do_shortcode('[woocommerce_product_search]');
      ?>
    </div>
    <div class="shoproll">

          <ul class="products">
               <?php
                  $args = array(
                    'post_type' => 'product',
                    'post_per_page' => 20
                  );
                  $loop = new WP_Query($args);
                  while( $loop->have_posts() ) : $loop->the_post();
                ?>
                <li>
                  <article>
                   <span class="cant">1</span>
                    <figure>
                    <a class="p-link" href="#" data-link="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                    </figure>
                    <div class="desc">
                      <h2><a class="p-link" href="#" data-link="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                      <p>
                          <?php
                              global $product;
                              $attributes = $product->get_attributes();
                              if ( $product->has_weight() ) {
                                  echo $product->get_weight();
                              }
                          ?>
                          Kg.
                      </p>
                    </div>
                    <div class="price-holder">
                      <div class="button-holder">
                        <?php
                          $id = get_the_ID();
                          $add = sprintf(
                            '[add_to_cart id="%1$s"]',
                            $id
                          );
                        ?>
                        <?php echo do_shortcode($add); ?>
                      </div>
                    </div>
                  </article>
                </li>
                <?php endwhile; wp_reset_query(); ?>

            </ul>

      <?php
        /**
         * woocommerce_after_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
          do_action( 'woocommerce_after_main_content' );
        ?>

      </div>
    <div id="holder">
      <div class="loader"></div>
    </div>
  </section>
  <div class="modal">
     <i class="fa fa-close"></i>
    <article class="p-container">

    </article>
  </div>
  </div>

<?php get_template_part('aside') ?>

<?php get_footer( 'shop' ); ?>
