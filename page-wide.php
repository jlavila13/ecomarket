<?php /*Template name: fullwidth*/ ?>
<?php get_header(); ?>
<main class="single fullwidth">
  <?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <section>
    <article>
      <div class="content">
        <?php the_content(); ?>
      </div>
    </article>
  </section>
  <?php endwhile; else : ?>
  <p><?php _e( 'Disculpa, no encontramos lo que buscas' ); ?></p>
  <?php endif; wp_reset_query();?>
</main>
<?php get_footer(); ?>
