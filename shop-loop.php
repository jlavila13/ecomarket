<?php
  $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
?>
      <ul class="products loop<?php echo $page; ?>">
           <?php
            $args = array(
              'post_type' => 'product',
              'post_per_page' => 12,
              'paged' => $page
            );
            $loop = new WP_Query($args);
            while( $loop->have_posts() ) : $loop->the_post();
         ?>
        <li>
          <article>
            <span class="cant">1</span>
            <figure>
            <a class="p-link" href="#" data-link="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            </figure>
            <div class="desc">
              <h2><a class="p-link" href="#" data-link="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
              <p>
                    <?php
                          global $product;
                          $attributes = $product->get_attributes();
                          if ( $product->has_weight() ) {
                              echo $product->get_weight();
                          }
                      ?>
                      Kg.
                </p>
            </div>
            <div class="price-holder">
              <div class="button-holder">
                <?php
                  $id = get_the_ID();
                  $add = sprintf(
                    '[add_to_cart id="%1$s"]',
                    $id
                  );
                ?>
                <?php echo do_shortcode($add); ?>
              </div>
            </div>
          </article>
        </li>
        <?php endwhile; wp_reset_query(); ?>
      </ul>
<!--/.products-->




