<?php
	//standarts
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
		define( 'TEMPPATH',  get_bloginfo('stylesheet_directory'));
		define( 'IMAGES', TEMPPATH. "/images/");
		define( 'SCRIPTS', TEMPPATH. "/scripts/");
		function new_excerpt_length($length) {
		    return 5;
		}

		add_filter('excerpt_length', 'new_excerpt_length');
		add_filter('widget_text', 'do_shortcode');
		add_theme_support( 'post-thumbnails' );

    //add_filter( 'the_title', 'wpse_75691_trim_words' );
    //function wpse_75691_trim_words( $title ){
        // limit words
        //return wp_trim_words( $title, 5, '...' );
    //}

  //menus
		add_action( 'init', 'register_my_menus' );
		function register_my_menus() {
			register_nav_menus(
				array(
					'main-nav' => __( 'Primary Menu' ),
					'self-nav' => __( 'Sec Menu' ),
					'cat_one' => __('cat_one'),
					'cat_two' => __('cat_two')
				)
			);
		}

  // menu principal
		function menu(){

			wp_nav_menu( array(
					'theme_location' => 'main-nav',
					'menu' => '',
					'container' => 'nav',
					'container_class' => 'menu-holder',
					'container_id' => '',
					'menu_class' => 'nav-menu',
					'menu_id' => '',
					'echo' => true,
					'fallback_cb' => 'wp_page_menu',
					'before' => '',
					'after' => '',
					'link_before' => '',
					'link_after' => '',
					'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
					'depth' => 0,
					'walker' => ''
			));

		}
		add_shortcode('main-nav', 'menu');

  // menu subhead
		function topmenu(){

			wp_nav_menu( array(
					'theme_location' => 'self-nav',
					'menu' => '',
					'container' => 'nav',
					'container_class' => 'menu-holder',
					'container_id' => '',
					'menu_class' => 'nav-menu',
					'menu_id' => '',
					'echo' => true,
					'fallback_cb' => 'wp_page_menu',
					'before' => '',
					'after' => '',
					'link_before' => '',
					'link_after' => '',
					'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
					'depth' => 0,
					'walker' => ''
			));

		}

		add_shortcode('top-nav', 'topmenu');


  // menu secundario
    function selfmenu(){

      wp_nav_menu( array(
          'theme_location' => 'self-nav',
          'menu' => '',
          'container' => 'nav',
          'container_class' => 'menu-holder',
          'container_id' => '',
          'menu_class' => 'nav-menu',
          'menu_id' => '',
          'echo' => true,
          'fallback_cb' => 'wp_page_menu',
          'before' => '',
          'after' => '',
          'link_before' => '',
          'link_after' => '',
          'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
          'depth' => 0,
          'walker' => ''
      ));

    }

    add_shortcode('self-nav', 'selfmenu');


  //sidebars

		add_action( 'widgets_init', 'theme_slug_widgets_init' );
		function theme_slug_widgets_init() {
			register_sidebar( array(
					'name' => __( 'Main Sidebar', 'theme-slug' ),
					'id' => 'sidebar-1',
					'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
					'before_widget' => '<li id="%1$s" class="widget %2$s">',
					'after_widget'  => '</li>',
					'before_title'  => '<h2 class="widgettitle">',
					'after_title'   => '</h2>',
				) );
		}

		add_action( 'widgets_init2', 'foot_area_one' );

		register_sidebar( array(
		    'id'          => 'f-area1',
		    'name'        => __( 'widget area1', $text_domain ),
		    'description' => __( 'area de widgets en el foot .', $text_domain ),
		) );

		register_sidebar( array(
				'id'          => 'f-area2',
				'name'        => __( 'widget area2', $text_domain ),
				'description' => __( 'segunda area de widgets en el foot .', $text_domain ),
		) );
		register_sidebar( array(
				'id'          => 'f-area3',
				'name'        => __( 'widget area3', $text_domain ),
				'description' => __( 'segunda area de widgets en el foot .', $text_domain ),
		) );
		register_sidebar( array(
				'id'          => 'f-area4',
				'name'        => __( 'widget area4', $text_domain ),
				'description' => __( 'segunda area de widgets en el foot .', $text_domain ),
		) );

		register_sidebar( array(
				'id'          => 'low-menu',
				'name'        => __( 'low widget area', $text_domain ),
				'description' => __( 'menu del low el foot .', $text_domain ),
		) );

		// cart
		register_sidebar( array(
				'id'          => 'side-cart',
				'name'        => __( 'sidecart area', $text_domain ),
				'description' => __( 'carrito lateral .', $text_domain ),
		) );

  //register scripts
		function my_scripts_method( $in_footer ) {
					wp_enqueue_style('awesomeFont', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
					wp_enqueue_style('flexstyle', get_stylesheet_directory_uri() . '/scripts/flexslider.css');
					wp_enqueue_style('flaticostyle', get_stylesheet_directory_uri() . '/fonts/flaticon.css');
					wp_enqueue_style('flexstyle', get_stylesheet_directory_uri() . '/fonts/wooicons.css');
					wp_enqueue_style('googlefonts', 'https://fonts.googleapis.com/css?family=Montserrat|Roboto:100,400');
					//scripts
          //wp_deregister_script('jquery');
					wp_enqueue_script('jqlib', '//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.js', array('jquery'), '', true);
					wp_enqueue_script('flexslider', get_stylesheet_directory_uri() . '/scripts/jquery.flexslider-min.js', array('jquery'), '', true);
					wp_enqueue_script('effects', get_stylesheet_directory_uri() . '/scripts/custom.js', array('jquery'), '', true);

		}
		add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

    wp_localize_script( 'ajax-pagination', 'ajaxpagination', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' )
    ));

  // woocommerce
		add_action( 'after_setup_theme', 'woocommerce_support' );
		    add_theme_support( 'woocommerce' );
        function woocommerce_support() {
		}

		add_action( 'wp_enqueue_scripts', 'wcqi_enqueue_polyfill' );
		function wcqi_enqueue_polyfill() {
		    wp_enqueue_script( 'wcqi-number-polyfill' );
		}

    //quantity input
      add_filter( 'woocommerce_loop_add_to_cart_link', 'quantity_inputs_for_woocommerce_loop_add_to_cart_link', 10, 2 );
      function quantity_inputs_for_woocommerce_loop_add_to_cart_link( $html, $product ) {
        if ( $product && $product->is_type( 'simple' ) && $product->is_purchasable() && $product->is_in_stock() && ! $product->is_sold_individually() ) {
          $html = '<form action="' . esc_url( $product->add_to_cart_url() ) . '" class="cart" method="post" enctype="multipart/form-data">';
          $html .= woocommerce_quantity_input( array(), $product, false );
          $html .= '<button type="submit" data-quantity="1" data-product_id="'. $prod .'" class="button alt ajax_add_to_cart add_to_cart_button product_type_simple">' . esc_html( $product->add_to_cart_text() ) . '</button>';
          $html .= '</form>';
        }
        return $html;
      }

  // shortcodes
		function catIco($class){
			foreach ((get_the_category()) as $category) {
				echo 'flaticon-' . $category -> slug;
			}
		}

		add_shortcode('cat-icon', 'catIco');


    //qunty

    add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

    function woocommerce_header_add_to_cart_fragment( $fragments ) {
      global $woocommerce;

      ob_start();

      ?>
      <a class="cart-customlocation" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
      <?php

      $fragments['a.cart-customlocation'] = ob_get_clean();

      return $fragments;

    }

    function mode_theme_update_mini_cart() {
      echo wc_get_template( 'cart/mini-cart.php' );
      die();
    }
    add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
    add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );


    wp_localize_script( 'ajax-pagination', 'ajaxpagination', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' )
    ));


    function wp_infinitepaginate(){
        get_template_part('shop-loop');
        die();

    }
    add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
    add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate');    // if user not logged in

?>

