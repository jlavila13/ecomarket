<?php /* Template name: homepage */ ?>

<?php get_header(); ?>

<main class="inicio">
  <div class="cover">
    <div class="cover-holder">
      <figure><img src="<?php print IMAGES; ?>basket.png" alt="comienza a comprar ahora"></figure>
      <div class="txt">
        <p>
          hortalizas frescas <br>
          del <span>d&iacute;a</span> en la <br>
          puerta de <span>tu casa</span>
        </p>
        <a class="button" href="<?php echo get_site_url(); ?>/shop" ><i class="flaticon-basket37"></i>hacer mercado</a>
      </div>
    </div>
  </div>
  <div class="cta">
    <section>
      <figure><a href="<?php echo get_site_url(); ?>/envios"><img src="<?php print IMAGES; ?>dest1.png" alt=""></a></figure>
      <figure><a href="<?php echo get_site_url(); ?>/shop"><img src="<?php print IMAGES; ?>dest2.png" alt=""></a></figure>
    </section>
  </div>
  <section>
    <h2>Para una vida m&aacute;s sana</h2>
    <div class="blogroll">
      <?php $args = array(
          'post_per_page' => 2
        );
        $query = new WP_Query($args);
            if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post();
      ?>
      <article>
        <i class="<?php echo do_shortcode('[cat-icon]'); ?>"></i>
        <figure>
          <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
        </figure>
        <div>
          <a href="<?php the_permalink(); ?>">
            <h3><?php the_title(); ?></h3>
            <p><?php the_excerpt(); ?></p>
          </a>
        </div>
      </article>
      <?php endwhile; else : ?>
      <p><?php _e( 'Disculpa, no encontramos lo que buscas' ); ?></p>
      <?php endif; ?>
    </div>
  </section>
  <div class="flexslider feat-slide">
    <h1 class="title">Productos destacados</h1>
    <ul class="slides">
      <?php
          $args = array(
            'post_type' => 'product',
            'post_per_page' => 15
          );
          $loop = new WP_Query($args);
          while( $loop->have_posts() ) : $loop->the_post();
       ?>
      <li>
        <article>
          <figure>
          <a class="p-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
          </figure>
          <div class="desc">
            <h2><a class="p-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <p>
            	    <?php
                        global $product;
                        $attributes = $product->get_attributes();
                        if ( $product->has_weight() ) {
                            echo $product->get_weight();
                        }
                    ?>
                    Kg.
            	</p>
          </div>
          <div class="price-holder">
            <div class="button-holder">
              <?php
                $id = get_the_ID();
                $add = sprintf(
                  '[add_to_cart id="%1$s"]',
                  $id
                );
              ?>
              <?php echo do_shortcode($add); ?>
            </div>
          </div>
        </article>
      </li>
    <?php endwhile; wp_reset_query(); ?>
    </ul>
  </div>
  <div class="modal">
     <i class="fa fa-close"></i>
    <article class="p-container">

    </article>
  </div>
</main>
<?php get_template_part('aside'); ?>
<?php get_footer(); ?>
